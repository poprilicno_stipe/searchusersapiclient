import Modal from "react-modal";
import { User } from "../../models/UserModel";
import "./Modal.css";

Modal.setAppElement("#root"); // to hide your application from assistive screenreaders and other assistive technologies while the modal is open. Use portals next time, Stipe.

function ModalUser(props: {
  user: User | null;
  modalIsOpen: boolean;
  setIsOpen: any;
}) {
  return (
    <div>
      <Modal
        aria-modal="true"
        aria-labelledby="dialogTitle"
        isOpen={props.modalIsOpen}
        onRequestClose={() => props.setIsOpen(false)}
        overlayClassName={{
          base: "overlay-base",
          afterOpen: "overlay-after",
          beforeClose: "overlay-before",
        }}
        className={{
          base: "content-base",
          afterOpen: "content-after",
          beforeClose: "content-before",
        }}
      >
        <h5 id="dialogTitle">User Details</h5>

        {(() => {
          if (props.user) {
            return (
              <ul className="green-blue-list">
                <li className="green-blue-list-item">
                  <label htmlFor="name">Name: </label>
                  <span id="name" className="listing">
                    {props.user.name}
                  </span>
                </li>
                <li className="green-blue-list-item">
                  <label htmlFor="username">Username: </label>
                  <span id="username" className="listing">
                    {props.user.username}
                  </span>
                </li>
                <li className="green-blue-list-item">
                  <label htmlFor="email">Email: </label>
                  <span id="email" className="listing">
                    {props.user.email}
                  </span>
                </li>
                <li className="green-blue-list-item">
                  <label htmlFor="website">Website: </label>
                  <span id="website" className="listing">
                    {props.user.website}
                  </span>
                </li>
              </ul>
            );
          } else {
            return <p>Nothing to show here... :/</p>;
          }
        })()}

        <button
          className="btn-outline btn-md"
          onClick={() => props.setIsOpen(false)}
        >
          Close
        </button>
      </Modal>
    </div>
  );
}

export default ModalUser;
