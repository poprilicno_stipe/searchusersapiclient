import "./Search.css";

function Search(prop: { setQuery: any; query: string }) {
    return (
      <form>
        <label htmlFor="search-form">
          <input
            aria-label="Search"
            type="search"
            name="search-form"
            id="search-form"
            className="input-primary input-md"
            placeholder="Search by name"
            value={prop.query}
            onChange={(e) => prop.setQuery(e.target.value)}
          />
        </label>
      </form>
    );
  }
  
  export default Search;
  