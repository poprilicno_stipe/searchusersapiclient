import { useState } from "react";
import { User } from "../../models/UserModel";
import Modal from "../modal/Modal";
import "./Listing.css";

function Listing(prop: { listing: User[] }) {
  const [selectedUser, setSelectedUser] = useState<User | null>(null);
  const [modalIsOpen, setIsOpen] = useState(false);

  const handleClickOnItem = (user: User) => () => {
    setSelectedUser(user);
    setIsOpen(true);
  };

  if (prop.listing.length < 1){
    return(
      <p className="green-blue-list"><strong>No results</strong></p>
    );
  }

  return (
    <>
      <ul className="green-blue-list">
        {prop.listing.map((user: User, index: number) => {
          return (
            <li
              key={index}
              onClick={handleClickOnItem(user)}
              className="green-blue-list-item"
            >
              <span className="listing">{user.name}</span>
            </li>
          );
        })}
      </ul>
      <Modal
        user={selectedUser}
        modalIsOpen={modalIsOpen}
        setIsOpen={setIsOpen}
      />
    </>
  );
}

export default Listing;
