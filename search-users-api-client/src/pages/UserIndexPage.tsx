import { useEffect, useState } from "react";
import "./UserIndexPage.css";
import Listing from "../components/listing/Listing";
import Search from "../components/search/Search";
import { User } from "../models/UserModel";

function UserIndexPage() {
  const [error, setError] = useState<Error>();
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState<User[]>([]);

  const [query, setQuery] = useState("");
  const [searchParam] = useState(["Search by name"]);

  function filterList(items: User[]) {
    return items.filter((item) => {
      return searchParam.some((newItem) => {
        return (
          item.name.toString().toLowerCase().indexOf(query.toLowerCase()) > -1
        );
      });
    });
  }

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((res) => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setItems(result);
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      );
  }, []);

  if (error) {
    return <>{error.message}</>;
  } else if (!isLoaded) {
    return <>loading...</>;
  } else {
    return (
      <article className="shadow-01dp">
        <h5>Search</h5>
        <Search setQuery={setQuery} query={query} />
        <h5>Results</h5>
        <Listing listing={filterList(items)} />
        <footer></footer>
      </article>
    );
  }
}

export default UserIndexPage;
