import "./App.css";
import UserIndexPage from "./pages/UserIndexPage";

function App() {
  return <UserIndexPage />;
}

export default App;
