# searchUsersApiClient

## Assigment:
Your task is to create a React application that will get a person's data from an API and
displays their names. Also, you should create a modal that will display only one person
(and their details) when the user clicks on a person’s name. You’ll find a list of
requirements and design guidelines below:  
- The user should see a list of all persons if the search box is empty.  
- The user should be able to filter people by their name.  
- The user should be able to see the results while typing on the search box, without
a form submit.  
- The user should see the message 'No results' if the filtering has no results.  
- The user should be able to close the modal.  

Please, follow the design and the specs you’ll find after the description.  
- The specs will help you implement our layout.  
- Use our CSS file below (see at the bottom description about the specifications):
https://s3.eu-central-1.amazonaws.com/dentolo-ui-elements/dist/css/main.css?v=
1.4  
- You can also add your own css when needed.  
- Display only 5 people from the placeholder API below:  
https://jsonplaceholder.typicode.com/users  
- You can use tools like Create React App, Webpack, Parcel Bundler, CodeSandbox
etc.  
- You should not use CSS Frameworks like Bootstrap. You can use CSS, Sass, Scss,
CSS-in-JS or CSS Modules.  
- The application should work properly on the latest version of Google Chrome.
BONUS  
- Implement your application with React Hooks.  
- Add animations. For example, animate the way results are changing or the way you
are displaying the modal.

![Assigment designs](./search-users-api-client/public/designs.jpg?raw=true "Assigment designs")

![Assigment specs](./search-users-api-client/public/specs.jpg?raw=true "Assigment specs")

## Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.  
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.  
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.  
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.  

### `npm run build`

Builds the app for production to the `build` folder.  
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.  
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
